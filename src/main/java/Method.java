import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Method {
    static ArrayList<Integer> listOfNumbers = new ArrayList<Integer>();

    static ArrayList<Integer> setInterval() {
        System.out.println("Enter start number:");
        Scanner scanner1 = new Scanner(System.in);
        int startNumber = scanner1.nextInt();
        System.out.println("Enter last number:");
        Scanner scanner2 = new Scanner(System.in);
        int lastNumber = scanner2.nextInt();
        for (int n = startNumber; n <= lastNumber; n++) {
            listOfNumbers.add(n);
        }
        System.out.println("_______________________________________________________________");
        System.out.print("Your list of numbers for " + startNumber + " to " + lastNumber + " : ");
        System.out.println(listOfNumbers);
        return listOfNumbers;
    }

    static void printOddNumbers() {
        System.out.print("Odd numbers: ");
        for (int n : listOfNumbers) {
            if (n % 2 != 0) {
                System.out.print(n + " ");
            }
        }
    }

    static void printOddNumbersReverse() {
        System.out.println(" ");
        System.out.print("Odd numbers reverse: ");
        Stream<Integer> stream = listOfNumbers.stream();
        List<Integer> collect = stream.sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        for (int n : collect) {
            if (n % 2 != 0) {
                System.out.print(n + " ");
            }
        }
    }

    static void sumOfOddNumbers() {
        int sumOdd = 0;
        for (int n : listOfNumbers) {
            if (n % 2 != 0) {
                sumOdd += n;
            }
        }
        System.out.println("Sum of odd numbers:");
        System.out.println(sumOdd);
    }

    static void sumOfEvenNumbers() {
        int sumEven = 0;
        for (int n : listOfNumbers) {
            if (n % 2 == 0) {
                sumEven += n;
            }
        }
        System.out.println("Sum of even numbers:");
        System.out.println(sumEven);
    }

    static void getFibonacciNumber(int sizeOfSet) {
        ArrayList<Integer> odd = new ArrayList<>();
        ArrayList<Integer> even = new ArrayList<>();
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.println("Fibonacci: ");
        System.out.println(n0 + " ");
        System.out.println(n1 + " ");
        for (int i = 3; i <= sizeOfSet; i++) {
            n2 = n0 + n1;
            System.out.print(n2 + " ");
            System.out.println();
            n0 = n1;
            n1 = n2;
            if (n2 % 2 != 0) {
                odd.add(n2);
            } else {
                even.add(n2);
            }
        }
        System.out.println("Max odd number (F1): ");

        try {
            System.out.println(odd.stream().max((o1, o2) -> o1 - o2).get());
        } catch (NoSuchElementException exeption) {
            System.out.println("_____ _ _____ _ _____ _");
            System.out.println("|Please enter number>3|");
            System.out.println("_____ _ _____ _ _____ _");
        }
        System.out.println("Max even number(F2): ");

        try {
            System.out.println(even.stream().max((o1, o2) -> o1 - o2).get());
        } catch (NoSuchElementException exeption1) {
            System.out.println("_____ _ _____ _ _____ _");
            System.out.println("|Please enter number>2|");
            System.out.println("_____ _ _____ _ _____ _");
        }

        int allSize = odd.size() + even.size() + 2;
        double oddPercent = (double) ((odd.size() + 2) * 100) / allSize;
        String oddPercentS = String.format("%.4g%n", oddPercent);
        System.out.print("");
        System.out.print("Percent of odd numbers: " + oddPercentS);
        double evenPercent = (double) (even.size() * 100) / allSize;
        String evenPercentS = String.format("%.4g%n", evenPercent);
        System.out.print("Percent of even numbers: " + evenPercentS);
    }
}