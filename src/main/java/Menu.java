import java.util.Scanner;

public class Menu {
    static int swValue;
    static int swValue2;
    static String exitSelected="Exit selected";
    static String invalidSelection ="Invalid selection, try again!!!";

    static void menu1() {
        System.out.println("|                        ^^|   MENU   |^^                      |");
        System.out.println("|______________________________________________________________|");
        System.out.println("|                                                              |");
        System.out.println("|       1. Enter the interval.(tasks with interval)            |");
        System.out.println("|       2. Enter the size of set(tasks with Fibonacci)         |");
        System.out.println("|       3. Exit                                                |");
        System.out.println("|                                                              |");
        System.out.println("|______________________________________________________________|");
        System.out.print("Please, choose an option:");
        Scanner scanner = new Scanner(System.in);
        swValue = scanner.nextInt();
    }

    static void menu2() {
        System.out.println(" ");
        System.out.println("                 Option 1 selected...");
        System.out.println("_______________________________________________________________");
        Method.setInterval();
        System.out.println(" ");
        System.out.println("_______________________________________________________________");
        System.out.println("||________________1. Print odd numbers_______________________||");
        System.out.println("||________________2. Print odd numbers reverse_______________||");
        System.out.println("||________________3. Sum of odd numbers______________________||");
        System.out.println("||________________4. Sum of even numbers_____________________||");
        System.out.println("||________________5. Exit____________________________________||");
        System.out.println("_______________________________________________________________");
        System.out.println("please, choose next step:");
        Scanner scanner1 = new Scanner(System.in);
        swValue2 = scanner1.nextInt();

    }

      static void menu3() {
          System.out.println("Option 2 selected");
          System.out.println("_______________________________________________________________");
          System.out.print("Please enter the size of set : ");
          Scanner scanner2 = new Scanner(System.in);
          int n = scanner2.nextInt();
          Method.getFibonacciNumber(n);
      }
}
