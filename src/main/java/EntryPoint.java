public class EntryPoint {

    public static void main(String[] args) {

        Menu.menu1();

        switch (Menu.swValue) {
            case 1:
                Menu.menu2();
                switch (Menu.swValue2) {
                    case 1:
                        Method.printOddNumbers();
                        break;
                    case 2:
                        Method.printOddNumbersReverse();
                        break;
                    case 3:
                        Method.sumOfOddNumbers();
                        break;
                    case 4:
                        Method.sumOfEvenNumbers();
                        break;
                    case 5:
                        System.out.println(Menu.exitSelected);
                        break;
                    default:
                        System.out.println(Menu.invalidSelection);
                }
                break;
            case 2:
                Menu.menu3();
                break;
            case 3:
                System.out.println(Menu.exitSelected);
                break;
            default:
                System.out.println(Menu.invalidSelection);
                break;
        }
    }
}
